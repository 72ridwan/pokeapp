import Pagination from "@/components/browse-page/Pagination";
import React, { useState } from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

function PaginationWrapper({ initPage = 0, totalPage = 10 }) {
  const [page, setPage] = useState(initPage);
  return (
    <Pagination
      page={page}
      itemsPerPage={10}
      totalItems={totalPage * 10}
      onChangePage={setPage}
    />
  );
}

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("can move to next page", () => {
  act(() => {
    render(<PaginationWrapper initPage={0} totalPage={10} />, container);
  });
  const nextPageButton = container.querySelector(
    'button[aria-label="Next page"]'
  );
  const pageInput = container.querySelector("input");

  act(() => {
    nextPageButton.dispatchEvent(new MouseEvent("click", { bubbles: true }));
  });
  expect(pageInput.value).toBe("2");
});

it("can only move in valid range", () => {
  act(() => {
    // Set page counter to be 4
    render(<PaginationWrapper initPage={3} totalPage={10} />, container);
  });
  const pageInput = container.querySelector("input");

  // Move beyond page 10
  act(() => {
    pageInput.focus();
    pageInput.value = "11";
    pageInput.blur();
  });
  // It stays in page 4
  expect(pageInput.value).toBe("4");

  // Move beyond page 1

  pageInput.value = "0";
  act(() => {
    pageInput.focus();
    pageInput.value = "0";
    pageInput.blur();
  });
  // It stays in page 4
  expect(pageInput.value).toBe("4");
});
