module.exports = {
  moduleNameMapper: {
    "@/components(.*)": "<rootDir>/components$1",
    "@/pages(.*)": "<rootDir>/pages$1",
    "@/assets(.*)": "<rootDir>/assets$1",
    "@/store(.*)": "<rootDir>/store$1",
    "@/utils(.*)": "<rootDir>/utils$1",
    "@/tests(.*)": "<rootDir>/tests$1",
  },
};
