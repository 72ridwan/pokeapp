Simple pokemon app made using NextJS + Apollo GraphQL.
This app uses API provided in here: https://graphql-pokeapi.vercel.app/api/graphql

## Demo

https://pokeapp-72ridwan.vercel.app/