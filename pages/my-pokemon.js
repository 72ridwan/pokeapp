import { css } from "@emotion/react";

import Head from "next/head";
import Navbar from "@/components/common/Navbar";

import CardList from "@/components/browse-page/CardList";
import NavbarTitle from "@/components/common/NavbarTitle";
import ListPageFooter from "@/components/ListPageFooter";
import MyPokemonCard from "@/components/my-pokemon/MyPokemonCard";
import useStore, { INDEX_NICKNAME, INDEX_NAME } from "@/store";
import Button from "@/components/common/Button";
import Link from "next/link";

export default function Home() {
  const userPokemons = useStore((state) => state.pokemons);
  const hasPokemon = userPokemons.length > 0;

  return (
    <>
      <Head>
        <title>My Pokemon - Pokeapp</title>
        <meta name="description" content="My pokemon" />
      </Head>

      <Navbar>
        <NavbarTitle>My Pokemon</NavbarTitle>
      </Navbar>

      <main
        css={css`
          overflow: auto;
          padding: 20px;
        `}
      >
        {hasPokemon && (
          <CardList>
            {userPokemons.map((pokemon) => {
              return (
                <MyPokemonCard
                  key={pokemon[INDEX_NICKNAME]}
                  name={pokemon[INDEX_NAME]}
                  nickname={pokemon[INDEX_NICKNAME]}
                />
              );
            })}
          </CardList>
        )}

        {!hasPokemon && (
          <div
            css={css`
              text-align: center;
              max-width: 300px;
              margin: auto;
              margin-top: 30px;
            `}
          >
            <img
              css={css`
                position: relative;
                left: 10px;
                width: 160px;
              `}
              src="/icons/empty-pokeball.svg"
            />
            <p
              css={css`
                color: gray;
              `}
            >
              Your list is empty.
              <br />
              Let's go catch some!
            </p>
            <Link href="/">
              <Button
                component="a"
                href="/"
                css={css`
                  display: inline-block;
                  width: auto;
                  margin: auto;
                `}
              >
                Browse pokemons
              </Button>
            </Link>
          </div>
        )}
      </main>

      <ListPageFooter />
    </>
  );
}
