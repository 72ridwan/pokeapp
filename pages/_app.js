import { useEffect, useState } from "react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import StoreUpdater from "../components/StoreUpdater";
import useStore, { initFromStorage } from "../store";
import "../styles/globals.css";

const client = new ApolloClient({
  uri: "https://graphql-pokeapi.vercel.app/api/graphql",
  cache: new InMemoryCache(),
});

function MyApp({ Component, pageProps }) {
  const initFunction = useStore((state) => state.init);
  const [shouldSave, setShouldSave] = useState(false);

  // Init store when component is ready on client side
  useEffect(() => {
    initFromStorage(initFunction);
    setShouldSave(true);
  }, [initFunction]);

  return (
    <ApolloProvider client={client}>
      <StoreUpdater shouldSave={shouldSave} />
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp;
