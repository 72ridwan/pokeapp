import { useEffect, useMemo, useReducer, useRef } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

import { gql, useQuery } from "@apollo/client";
import { css } from "@emotion/react";
import { useRouter } from "next/router";

import kebabToTitleCase from "@/utils/kebabToTitleCase";
import getCatchStatus from "@/utils/getCatchStatus";

import Head from "next/head";
import Navbar from "@/components/common/Navbar";
import Button from "@/components/common/Button";

import PokemonDetailImage from "@/components/pokemon-detail/PokemonDetailImage";
import PokemonSuccessDialog from "@/components/pokemon-detail/PokemonSuccessDialog";
import PokemonFailDialog from "@/components/pokemon-detail/PokemonFailDialog";
import NavbarTitle from "@/components/common/NavbarTitle";
import IconButton from "@/components/common/IconButton";
import FooterBar from "@/components/common/FooterBar";
import Skeleton from "@/components/common/Skeleton";

const GET_POKEMON = gql`
  query($name: String!) {
    pokemon(name: $name) {
      name
      sprites {
        front_default
      }
      types {
        type {
          name
        }
      }
      moves {
        move {
          name
        }
      }
    }
  }
`;

const sectionTitleStyle = css`
  margin: 0;
  margin-top: 10px;
`;
const listStyle = css`
  display: flex;
  list-style-type: none;
  margin-block-start: 0;
  margin-block-end: 0;
  margin-inline-start: 0;
  margin-inline-end: 0;
  padding-inline-start: 0;
  word-break: break-word;
  flex-wrap: wrap;
  margin: 0 -4px;

  & > * {
    padding: 5px;
    border-radius: 4px;
    margin: 4px;
    color: white;
  }
`;

/**
 * Returns a hue degree in 5 color choices.
 */
function getTagHueDegree(val, i) {
  return [17, 233, 282, 318, 133][i % 5];
}

function catchReducer(state, action) {
  if (action.type === "start") {
    return {
      catching: true,
      catched: null,
      name: null,
    };
  }
  if (action.type === "fail") {
    return {
      catching: false,
      catched: false,
      name: null,
    };
  }
  if (action.type === "success") {
    return {
      catching: false,
      catched: true,
      name: action.name,
    };
  }
  if (action.type === "reset") {
    return {
      catching: false,
      catched: null,
      name: null,
    };
  }
}

function useCheckMounted() {
  const isMountedRef = useRef(true);
  useEffect(() => {
    return () => {
      isMountedRef.current = false;
    };
  }, []);
  return isMountedRef;
}

async function wait(miliseconds = 1000) {
  return new Promise((res) => {
    setTimeout(res, miliseconds);
  });
}

export default function PokemonDetail({ name }) {
  const router = useRouter();
  const pokemonName = router.isReady ? (router.query.name || "").trim() : name;

  const [catchingState, dispatchCatching] = useReducer(catchReducer, {
    catching: false,
    catched: null,
    name: null,
  });

  // Reset the state on component mount
  useEffect(() => {
    dispatchCatching({ type: "reset" });
  }, []);

  const isMounted = useCheckMounted();

  const { loading, error, data, refetch } = useQuery(GET_POKEMON, {
    variables: { name: pokemonName },
  });

  const pokemon = data?.pokemon;
  const dataExists = !!pokemon?.name;

  const pokemonTypeNames = useMemo(() => {
    const pokemonTypes = pokemon?.types || [];
    return pokemonTypes
      .map((type) => {
        return kebabToTitleCase(type.type.name);
      })
      .sort();
  }, [pokemon]);

  const pokemonMoveNames = useMemo(() => {
    const pokemonMoves = pokemon?.moves || [];
    return pokemonMoves
      .map((move) => {
        return kebabToTitleCase(move.move.name);
      })
      .sort();
  }, [pokemon]);

  const pokemonTypeBgHues = useMemo(() => {
    return new Array(pokemonTypeNames.length).fill(null).map(getTagHueDegree);
  }, [pokemonTypeNames.length]);

  const pokemonMoveBgHues = useMemo(() => {
    return new Array(pokemonMoveNames.length).fill(null).map(getTagHueDegree);
  }, [pokemonMoveNames.length]);

  const catchPokemon = async () => {
    dispatchCatching({ type: "start" });
    await wait();
    const success = getCatchStatus();

    if (!isMounted.current) {
      return;
    }
    if (success) {
      dispatchCatching({ type: "success", name: pokemon.name });
    } else {
      dispatchCatching({ type: "fail" });
    }
  };

  const pokemonSuccessDialogRef = useRef(null);
  const pokemonFailDialogRef = useRef(null);

  useEffect(() => {
    if (catchingState.catched === null) {
      return;
    }

    if (catchingState.catched) {
      pokemonSuccessDialogRef.current.openDialog();
    } else {
      pokemonFailDialogRef.current.openDialog();
    }
  }, [catchingState.catched]);

  return (
    <>
      <Head>
        <title>{kebabToTitleCase(pokemonName)} - Pokeapp</title>
        <meta name="description" content="Pokeapp" />
      </Head>

      <Navbar>
        <IconButton
          color="white"
          css={css`
            margin: 0;
            margin-right: 8px;
            margin-left: -10px;
          `}
          onClick={() => {
            router.push("/");
          }}
        >
          <FontAwesomeIcon icon={faChevronLeft} />
        </IconButton>
        <NavbarTitle>{kebabToTitleCase(pokemonName)}</NavbarTitle>
      </Navbar>

      <main
        css={css`
          padding: 20px;
        `}
      >
        {dataExists && (
          <>
            <div
              css={css`
                display: flex;
                flex-flow: column;

                max-width: 1200px;
                margin: auto;

                @media (min-width: 768px) {
                  flex-flow: row;
                }
              `}
            >
              <div
                css={css`
                  margin: 0 -20px;

                  @media (min-width: 768px) {
                    margin: 0;
                    margin-right: 20px;
                  }
                `}
              >
                <PokemonDetailImage src={pokemon.sprites.front_default} />
              </div>

              <div css={css``}>
                <h4 css={sectionTitleStyle}>Types</h4>
                <ul css={listStyle}>
                  {pokemonTypeNames.map((typeName, idx) => {
                    // Use inline style to prevent creating multiple CSS class just to change color
                    return (
                      <li
                        key={idx}
                        style={{
                          backgroundColor: `hsl(${pokemonTypeBgHues[idx]}deg 61% 26%)`,
                        }}
                      >
                        {typeName}
                      </li>
                    );
                  })}
                </ul>

                <h4 css={sectionTitleStyle}>Moves:</h4>
                <ul css={listStyle}>
                  {pokemonMoveNames.map((moveName, idx) => {
                    return (
                      <li
                        key={idx}
                        style={{
                          backgroundColor: `hsl(${pokemonMoveBgHues[idx]}deg 61% 26%)`,
                        }}
                      >
                        {moveName}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>

            <PokemonSuccessDialog
              ref={pokemonSuccessDialogRef}
              pokemonName={pokemon.name}
              onCancel={() => dispatchCatching({ type: "reset" })}
              onSave={() => dispatchCatching({ type: "reset" })}
            />
            <PokemonFailDialog
              ref={pokemonFailDialogRef}
              pokemonName={pokemon.name}
              onCancel={() => dispatchCatching({ type: "reset" })}
              onRetry={() => catchPokemon()}
            />
          </>
        )}

        {!loading && !error && !dataExists && <div>Pokemon not found.</div>}

        {!loading && error && (
          <div
            css={css`
              text-align: center;
            `}
          >
            <p>Oops, something went wrong.</p>
            <Button
              css={css`
                margin: auto;
              `}
              onClick={() => refetch()}
            >
              Try again
            </Button>
          </div>
        )}

        {loading && (
          <div
            css={css`
              display: flex;
              flex-flow: column;

              max-width: 1200px;
              margin: auto;

              @media (min-width: 768px) {
                flex-flow: row;
              }
            `}
          >
            <div
              css={css`
                margin: 0 -20px;

                @media (min-width: 768px) {
                  margin: 0;
                  margin-right: 20px;
                }
              `}
            >
              <PokemonDetailImage loading />
            </div>

            <div css={css``}>
              <Skeleton css={sectionTitleStyle}>
                <span>Types</span>
              </Skeleton>
              <ul css={listStyle}>
                {new Array(2).fill("loading").map((typeName, idx) => {
                  return (
                    <li key={idx}>
                      <Skeleton>
                        <span>Loading</span>
                      </Skeleton>
                    </li>
                  );
                })}
              </ul>

              <div
                css={css`
                  height: 20px;
                `}
              />

              <Skeleton css={sectionTitleStyle}>
                <span>Moves</span>
              </Skeleton>

              <ul css={listStyle}>
                {new Array(10).fill("loading").map((moveName, idx) => {
                  return (
                    <li key={idx}>
                      <Skeleton>
                        <span>Loading</span>
                      </Skeleton>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        )}
      </main>

      {dataExists && (
        <FooterBar footerHeight={74}>
          {catchingState.catching ? (
            <div
              css={css`
                font-size: 24px;
                margin: 0 auto;
                padding: 8px 16px;
                text-align: center;
              `}
            >
              Catching...
            </div>
          ) : (
            <Button
              css={css`
                font-size: 24px;
                margin: 0 auto;
                padding: 8px 16px;
              `}
              onClick={catchPokemon}
            >
              Catch it!
            </Button>
          )}
        </FooterBar>
      )}
    </>
  );
}

export function getServerSideProps({ params }) {
  const name = (params.name || "").trim();
  return { props: { name }, notFound: !name };
}
