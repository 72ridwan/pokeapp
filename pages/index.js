import { useQuery, gql } from "@apollo/client";
import { css } from "@emotion/react";

import Head from "next/head";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Button from "@/components/common/Button";
import Skeleton from "@/components/common/Skeleton";
import Navbar from "@/components/common/Navbar";

import BrowsePokemonItem from "@/components/browse-page/BrowsePokemonItem";
import CardContent from "@/components/browse-page/CardContent";
import CardImage from "@/components/browse-page/CardImage";
import CardItem from "@/components/browse-page/CardItem";
import CardList from "@/components/browse-page/CardList";
import Pagination from "@/components/browse-page/Pagination";
import NavbarTitle from "@/components/common/NavbarTitle";
import ListPageFooter from "@/components/ListPageFooter";

const GET_POKEMONS = gql`
  query($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      results {
        image
        name
      }
    }
  }
`;

export default function Home({ startPage }) {
  const router = useRouter();

  const [perPage] = useState(12);
  const [page, setPage] = useState(() => {
    let pageQuery = router.isReady ? router.query.page : startPage;
    pageQuery = parseInt(pageQuery, 10);
    return Number.isNaN(pageQuery) ? 0 : pageQuery - 1;
  });
  const [totalCount, setTotalCount] = useState(0);

  // Sync route param with page state
  const routerReplace = router.replace;
  useEffect(() => {
    routerReplace("/?page=" + (page + 1), undefined, { shallow: true });
  }, [page]);

  const { loading, error, data, refetch } = useQuery(GET_POKEMONS, {
    variables: { limit: perPage, offset: page * perPage },
  });

  useEffect(() => {
    if (!loading && data) {
      setTotalCount(data?.pokemons?.count || 0);
    }
  }, [data]);

  return (
    <>
      <Head>
        <title>Browse - Pokeapp</title>
        <meta name="description" content="Pokeapp" />
      </Head>

      <Navbar>
        <NavbarTitle>Browse</NavbarTitle>
      </Navbar>

      <main
        css={css`
          overflow: auto;
          padding: 20px;
        `}
      >
        <Pagination
          page={page}
          onChangePage={(page) => setPage(page)}
          itemsPerPage={perPage}
          totalItems={totalCount}
          css={css`
            margin-bottom: 20px;
          `}
        />

        {!loading && !error && (
          <CardList>
            {data.pokemons.results.map((pokemon) => {
              return (
                <BrowsePokemonItem
                  key={pokemon.name}
                  name={pokemon.name}
                  image={pokemon.image}
                />
              );
            })}
          </CardList>
        )}

        {loading && (
          <CardList>
            {new Array(12).fill(null).map((val, idx) => {
              return (
                <CardItem key={idx}>
                  <Skeleton
                    css={css`
                      width: 100%;
                    `}
                  >
                    <CardImage />
                  </Skeleton>
                  <CardContent>
                    <Skeleton
                      css={css`
                        height: 20px;
                        margin-bottom: 8px;
                        width: 5em;
                        display: block;
                      `}
                    />
                    <Skeleton
                      css={css`
                        height: 14px;
                        display: block;
                        width: 3em;
                        // Compensate for "More >" button
                        margin-bottom: 22px;
                      `}
                    />
                  </CardContent>
                </CardItem>
              );
            })}
          </CardList>
        )}

        {error && (
          <div
            css={css`
              text-align: center;
            `}
          >
            Sorry, something went wrong.
            <br />
            <Button
              onClick={() =>
                refetch({ limit: perPage, offset: perPage * page })
              }
              css={css`
                margin: 10px auto;
              `}
            >
              Refresh
            </Button>
          </div>
        )}

        <Pagination
          page={page}
          onChangePage={(page) => setPage(page)}
          itemsPerPage={perPage}
          totalItems={totalCount}
          hidePage
          css={css`
            margin-top: 20px;
          `}
        />
      </main>

      <ListPageFooter />
    </>
  );
}

export function getServerSideProps({ params }) {
  const startPage = (params?.page || "").trim();
  return { props: { startPage } };
}
