import create from "zustand";

export const INDEX_NAME = 0;
export const INDEX_NICKNAME = 1;
const STORE_KEY = "pokemons";

/**
 * Store for pokemon types, where `pokemons` is array containing
 * array of pokemon's name and nicknames.
 */

const useStore = create((set) => ({
  pokemons: [],
  loading: false,

  add: (name, nickname) => {
    let success = false;

    set((state) => {
      // Create full copy of state.pokemons
      const pokemons = [...state.pokemons];
      const pokemonIndex = pokemons.findIndex((pokemon) => {
        return pokemon[INDEX_NICKNAME] === nickname;
      });
      const nicknameExists = pokemonIndex >= 0;

      if (nicknameExists) {
        return;
      }

      pokemons.push([name, nickname]);
      success = true;
      return { pokemons };
    });

    return success;
  },

  remove: (nickname) => {
    let success = false;

    set((state) => {
      // Create full copy of state.pokemons
      const pokemons = [...state.pokemons];
      const deletedIndex = pokemons.findIndex((pokemon) => {
        return pokemon[INDEX_NICKNAME] === nickname;
      });
      const nicknameExists = deletedIndex >= 0;
      if (!nicknameExists) {
        return;
      }
      pokemons.splice(deletedIndex, 1);
      success = true;
      return { pokemons };
    });

    return success;
  },

  /** Initialize user pokemon array */
  init: (pokemons = []) => {
    set(() => {
      return { pokemons };
    });
  },
}));

/** Initialize store from localStorage.
 * If the format in storage is invalid, it is discarded. */

export function initFromStorage(storeInitFunction) {
  let pokemons;
  try {
    pokemons = JSON.parse(localStorage.getItem(STORE_KEY));
    localStorage.removeItem(STORE_KEY);
  } catch {
    return;
  }

  // Make sure it's 2 dimensional array
  if (!Array.isArray(pokemons)) {
    return;
  }

  for (let i = 0; i < pokemons.length; i++) {
    const pokemon = pokemons[i] || [];
    if (typeof pokemon[0] !== "string" || typeof pokemon[1] !== "string") {
      return;
    }
  }

  storeInitFunction(pokemons);
}

export function updateStorage(pokemons) {
  localStorage.setItem(STORE_KEY, JSON.stringify(pokemons));
}

export default useStore;
