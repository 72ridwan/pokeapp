export default function getCatchStatus() {
  return Math.random() <= 0.5;
}
