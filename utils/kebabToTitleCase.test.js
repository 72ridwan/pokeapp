import kebabToTitleCase from "./kebabToTitleCase";

it("handle single word", () => {
  expect(kebabToTitleCase("bulbasaur")).toBe("Bulbasaur");
  expect(kebabToTitleCase("a")).toBe("A");
});

it("handle multiple words", () => {
  expect(kebabToTitleCase("giratina-altered")).toBe("Giratina Altered");
  expect(kebabToTitleCase("wormadam-plant")).toBe("Wormadam Plant");
});

it("handle empty words", () => {
  expect(kebabToTitleCase("")).toBe("");
  expect(kebabToTitleCase("-----")).toBe("");
  expect(kebabToTitleCase("--test-string--")).toBe("Test String");
});
