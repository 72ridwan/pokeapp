function toStartCase(word) {
  if (word.length < 0) {
    return word;
  }
  return word[0].toUpperCase() + word.substring(1);
}

export default function kebabToTitleCase(input) {
  return (
    input
      .split(/-+/)
      // Filter empty words
      .filter((word) => !!word)
      .map(toStartCase)
      .join(" ")
  );
}
