import { css } from "@emotion/react";
import { useRouter } from "next/router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark as FaBookmarkRegularIcon } from "@fortawesome/free-regular-svg-icons";
import { faBookmark as FaBookmarkSolidIcon } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import FooterBar from "@/components/common/FooterBar";

import PokeballRegularIcon from "@/assets/icons/pokeball-regular.svg";
import PokeballSolidIcon from "@/assets/icons/pokeball-solid.svg";

const iconStyle = css`
  // Need important to override FontAwesome settings
  width: 24px !important;
  height: 24px !important;
  display: block;
  margin: auto;
`;

const titleStyle = css`
  margin: 0;
  font-size: 16px;
  font-weight: normal;
`;

const menuItemStyle = css`
  flex-basis: 50%;
  text-align: center;
  display: flex;
  flex-flow: column;
  color: #65686b;
  background: white;
  padding: 10px;
  padding-top: 5px;
  transition: 0.1s background ease;

  &:focus {
    outline: none;
    background: #d3d3d3;
  }

  &.active {
    color: #084c7c;
    & .css-${titleStyle.name} {
      font-weight: bold;
    }
  }
`;

function ListPageMenuItem({ href, renderIcon, title, ...props }) {
  const router = useRouter();
  const active = router.pathname === href;
  return (
    <Link {...props} href={href}>
      <a href={href} css={menuItemStyle} className={active ? "active" : ""}>
        {renderIcon({ css: iconStyle, active })}
        <span css={titleStyle}>{title}</span>
      </a>
    </Link>
  );
}

export default function ListPageFooter() {
  return (
    <FooterBar
      footerHeight={74}
      css={css`
        display: flex;
        padding: 0;
      `}
    >
      <div
        css={css`
          display: flex;
          flex-flow: row;
          width: 100%;
          @media (min-width: 600px) {
            width: 300px;
            margin: 0 auto;
          }
        `}
      >
        <ListPageMenuItem
          href="/"
          title="Browse"
          renderIcon={({ css, active }) =>
            active ? (
              <PokeballSolidIcon css={css} />
            ) : (
              <PokeballRegularIcon css={css} />
            )
          }
        />

        <div
          css={css`
            @media (min-width: 600px) {
              border-left: thin solid #00000040;
              margin: 10px 0;
            }
          `}
        />

        <ListPageMenuItem
          href="/my-pokemon"
          title="My Pokemon"
          renderIcon={({ css, active }) => (
            <FontAwesomeIcon
              css={css}
              icon={active ? FaBookmarkSolidIcon : FaBookmarkRegularIcon}
            />
          )}
        />
      </div>
    </FooterBar>
  );
}
