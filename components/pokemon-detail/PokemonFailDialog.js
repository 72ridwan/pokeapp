import { forwardRef, useImperativeHandle, useState } from "react";
import { css } from "@emotion/react";
import kebabToTitleCase from "@/utils/kebabToTitleCase";

import Modal from "@material-ui/core/Modal";
import Button from "../common/Button";

function PokemonFailDialog({ pokemonName, onCancel, onRetry }, ref) {
  const [open, setOpen] = useState(false);

  useImperativeHandle(
    ref,
    () => {
      return {
        openDialog: () => {
          setOpen(true);
        },
      };
    },
    []
  );

  return (
    <Modal open={open}>
      <div
        css={css`
          display: flex;
          flex-flow: row;
          align-items: center;
          justify-content: center;
          width: 100%;
          height: 100%;
          padding: 23px;
        `}
      >
        <div
          css={css`
            background: #ffffff;
            box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            padding: 23px;
            text-align: center;
          `}
        >
          <h4
            css={css`
              margin: 0;
              margin-bottom: 16px;
              font-size: 24px;
            `}
          >
            Oh no!
            <br />
            {kebabToTitleCase(pokemonName)} didn't get caught{" "}
            <span
              css={css`
                white-space: nowrap;
              `}
            >
              :(
            </span>
          </h4>

          <div
            css={css`
              display: flex;
              align-items: center;
            `}
          >
            <Button
              color="black"
              onClick={() => {
                setOpen(false);
                onCancel();
              }}
              css={css`
                margin-left: auto;
                margin-right: 4px;
                width: 6rem;
              `}
            >
              Close
            </Button>
            <Button
              css={css`
                margin-left: 4px;
                margin-right: auto;
                width: 6rem;
              `}
              onClick={() => {
                setOpen(false);
                onRetry();
              }}
            >
              Try Again
            </Button>
          </div>
        </div>
      </div>
    </Modal>
  );
}

export default forwardRef(PokemonFailDialog);
