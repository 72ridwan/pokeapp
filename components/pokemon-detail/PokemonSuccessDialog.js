import { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { css } from "@emotion/react";
import kebabToTitleCase from "@/utils/kebabToTitleCase";

import Modal from "@material-ui/core/Modal";

import useStore from "../../store";
import Button from "../common/Button";
import Input from "../common/Input";

const hideable = css`
  opacity: 0;
  transition: 0.1s opacity ease;
`;
const hideableShow = css`
  opacity: 1;
`;

class ValidationError extends Error {}

function validateNickname(nicknameInput) {
  let nicknameValue = nicknameInput;

  if (!nicknameValue.match(/[A-z]/)) {
    throw new ValidationError("Don't forget to add the letters");
  }
  if (!nicknameValue.match(/^[A-z-]+$/)) {
    throw new ValidationError("Use letters and dash only, please");
  }

  nicknameValue = nicknameValue.trim();
  if (!nicknameValue) {
    throw new ValidationError("Hey, did you forgot to give it a nickname?");
  }
  if (nicknameValue.length > 96) {
    throw new ValidationError(
      "This name is too long, please choose shorter name"
    );
  }
  if (nicknameValue.length < 4) {
    throw new ValidationError("Name must be at least 4 characters");
  }

  return nicknameValue;
}

function PokemonSuccessDialog({ pokemonName, onCancel, onSave }, ref) {
  const [open, setOpen] = useState(false);
  const inputRef = useRef(null);

  const addPokemon = useStore((state) => state.add);
  const [errorMessage, setErrorMessage] = useState(null);

  useImperativeHandle(
    ref,
    () => {
      return {
        openDialog: () => {
          setErrorMessage(null);
          setOpen(true);
        },
      };
    },
    []
  );

  return (
    <Modal open={open}>
      <div
        css={css`
          background: #ffffff;
          box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          overflow: hidden;
          position: fixed;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          padding: 23px;
          text-align: center;
        `}
      >
        <h4
          css={css`
            font-size: 20px;
            margin: 0;
          `}
        >
          You've caught
        </h4>
        <div
          css={css`
            display: inline-block;
            background: #ae025c;
            position: relative;
            padding: 5px 8px;
            margin: 8px 0 20px;
            word-break: break-word;
          `}
        >
          <span
            css={css`
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
              background: #feb940;
              z-index: 0;
              transform: skew(-5.7deg, -1.4deg);
            `}
          />
          <h3
            css={css`
              position: relative;
              color: #ae025c;
              z-index: 1;
              font-size: 30px;
              margin: 0;
            `}
          >
            {kebabToTitleCase(pokemonName)}!
          </h3>
        </div>

        <div>What should we name it?</div>
        <Input
          css={css`
            margin-top: 8px;
          `}
          ref={inputRef}
          max={96}
        />

        <div
          css={css`
            ${hideable};
            ${errorMessage && hideableShow};
            color: red;
            padding: 21px;
          `}
        >
          {errorMessage}
        </div>

        <div
          css={css`
            display: flex;
            align-items: center;
          `}
        >
          <Button
            color="black"
            onClick={() => {
              onCancel();
              setOpen(false);
            }}
            css={css`
              margin-left: auto;
              margin-right: 4px;
              width: 4rem;
            `}
          >
            Cancel
          </Button>
          <Button
            css={css`
              margin-left: 4px;
              margin-right: auto;
              width: 4rem;
            `}
            onClick={() => {
              let pokemonNickname;
              try {
                pokemonNickname = validateNickname(inputRef.current.value);
              } catch (e) {
                if (e instanceof ValidationError) {
                  setErrorMessage(e.message);
                } else {
                  setErrorMessage(
                    "Sorry, something is wrong. Please try again."
                  );
                }
                return;
              }

              const success = addPokemon(pokemonName, pokemonNickname);

              if (success) {
                setErrorMessage(null);
                onSave();
                setOpen(false);
              } else {
                setErrorMessage(
                  `Whoops! You have already pokemon named ${pokemonNickname}`
                );
              }
            }}
          >
            Save
          </Button>
        </div>
      </div>
    </Modal>
  );
}

export default forwardRef(PokemonSuccessDialog);
