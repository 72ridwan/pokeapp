import { useState } from "react";
import { css, keyframes } from "@emotion/react";
import Skeleton from "../common/Skeleton";

const changePokemon = keyframes`
  0% {
    top: 0;
  }
  33.299% {
    top: 0;
  }
  33.3% {
    top: -100%;
  }
  66.699% {
    top: -100%;
  }
  66.7% {
    top: -200%;
  }
  100% {
    top: -200%;
  }
`;

const mainSpriteStyle = css`
  top: 0;
  left: 0;
  position: absolute;
  height: 192px;
  width: 192px;
  image-rendering: pixelated;
`;

const outerContainerStyle = css`
  position: relative;
  height: calc(96px * 3);
  width: calc(96px * 3);
  margin: 10px auto;

  @media (min-width: 768px) {
    height: calc(96px * 4);
    width: calc(96px * 4);
  }
`;

const innerAbsoluteFullStyle = css`
  top: 0;
  left: 0;
  position: absolute;
  height: 192px;
  width: 192px;
`;

// Inner container will have height 192 px but then zoomed
// to match outer container style.

const innerContainerStyle = css`
  ${innerAbsoluteFullStyle};
  border-radius: 100%;
  transform: scale(calc(96 * 3 / 192));
  transform-origin: top left;

  @media (min-width: 768px) {
    transform: scale(calc(96 * 4 / 192));
  }
`;

export default function PokemonDetailImage({ src, loading }) {
  const [spriteBgHue] = useState(() => {
    return parseInt(Math.random() * 360, 10);
  });

  return (
    <div css={outerContainerStyle}>
      <div
        css={innerContainerStyle}
        style={{ backgroundColor: `hsl(${spriteBgHue}deg 75% 86%)` }}
      >
        {loading && (
          <div
            css={[
              innerAbsoluteFullStyle,
              css`
                overflow: hidden;
              `,
            ]}
          >
            <Skeleton
              css={[
                mainSpriteStyle,
                css`
                  border-radius: 100%;
                `,
              ]}
            />
            <img
              src="/pokemon-anonymous.svg"
              css={[
                mainSpriteStyle,
                css`
                  filter: blur(2px);
                  height: calc(192px * 3);
                  animation: ${changePokemon} calc(1.3s * 3) 0.6s linear
                    infinite;
                `,
              ]}
            />
          </div>
        )}
        {!loading && <img css={mainSpriteStyle} src={src} />}
      </div>
    </div>
  );
}
