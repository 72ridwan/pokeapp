import { useEffect } from "react";
import useStore, { updateStorage } from "@/store";

/**
 * This component is used to listen to pokemon state changes
 * and then update it into localStorage.
 * It only updates when shouldSave is true.
 */
export default function StoreUpdater({ shouldSave }) {
  const pokemons = useStore((state) => state.pokemons);
  useEffect(() => {
    if (shouldSave) {
      updateStorage(pokemons);
    }
  }, [shouldSave, pokemons]);

  return null;
}
