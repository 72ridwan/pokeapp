import { css } from "@emotion/react";

import Skeleton from "@/components/common/Skeleton";
import CardContent from "@/components/browse-page/CardContent";
import CardImage from "@/components/browse-page/CardImage";
import CardItem from "@/components/browse-page/CardItem";

export default function MyPokemonCardLoading() {
  return (
    <CardItem>
      <Skeleton
        css={css`
          width: 100%;
        `}
      >
        <CardImage />
      </Skeleton>
      <CardContent>
        <Skeleton
          css={css`
            height: 20px;
            margin-bottom: 8px;
            width: 5em;
            display: block;
          `}
        />
        <Skeleton
          css={css`
            height: 14px;
            display: block;
            width: 3em;
            // Compensate for "More >" button
            margin-bottom: 22px;
          `}
        />
      </CardContent>
    </CardItem>
  );
}
