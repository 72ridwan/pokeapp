import { gql, NetworkStatus, useQuery } from "@apollo/client";
import { css } from "@emotion/react";

import kebabToTitleCase from "@/utils/kebabToTitleCase";

import CardContent from "../browse-page/CardContent";
import CardItem from "../browse-page/CardItem";
import CardImage from "../browse-page/CardImage";
import Button from "../common/Button";
import useStore from "@/store";
import MyPokemonCardLoading from "./MyPokemonCardLoading";

const titleStyle = css`
  font-weight: bold;
  font-size: 20px;
  line-height: 1;
  margin: 0;
  margin-bottom: 4px;
`;

const cardItemStyle = css`
  transition: 0.1s background, 0.1s transform ease;
  word-break: break-word;
  user-select: none;
`;

const errorCardContentStyle = css`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
`;

const errorCardNoticeStyle = css`
  color: gray;
  text-align: center;
`;

export const GET_POKEMON = gql`
  query($name: String!) {
    pokemon(name: $name) {
      name
      sprites {
        front_default
      }
    }
  }
`;

export default function MyPokemonCard({ name, nickname }) {
  const { loading, data, error, refetch, networkStatus } = useQuery(
    GET_POKEMON,
    {
      variables: { name },
    }
  );

  const pokemonNameExists = !loading && data?.pokemon?.name;
  const image = data?.pokemon?.sprites?.front_default;

  const removePokemon = useStore((state) => state.remove);
  if (loading || networkStatus === NetworkStatus.refetch) {
    return <MyPokemonCardLoading />;
  }

  if (error) {
    return (
      <CardItem css={cardItemStyle}>
        <CardContent css={errorCardContentStyle}>
          <span css={errorCardNoticeStyle} id="error-notice">
            Something went wrong.
          </span>
          <Button
            id="refetch"
            color="black"
            css={css`
              margin: 8px auto;
            `}
            onClick={() => {
              refetch();
            }}
          >
            Try again
          </Button>
        </CardContent>
      </CardItem>
    );
  }

  if (!pokemonNameExists) {
    return (
      <CardItem css={cardItemStyle}>
        <CardContent css={errorCardContentStyle}>
          <span css={errorCardNoticeStyle} id="error-notice">
            This pokemon can't be displayed.
          </span>
          <Button
            id="delete-pokemon"
            color="black"
            css={css`
              margin: 8px auto;
            `}
            onClick={() => {
              removePokemon(nickname);
            }}
          >
            Delete
          </Button>
        </CardContent>
      </CardItem>
    );
  }

  return (
    <CardItem css={cardItemStyle}>
      <CardImage src={image} />
      <CardContent
        css={css`
          flex-grow: 1;
          display: flex;
          flex-flow: column;
        `}
      >
        <h5 css={titleStyle}>{nickname}</h5>
        <div
          css={css`
            color: rgba(0, 0, 0, 0.5);
          `}
        >
          {kebabToTitleCase(name)}
        </div>
        <div
          css={css`
            flex-grow: 1;
            padding: 2px;
          `}
        />
        <Button
          css={css`
            margin: 8px 0 0 auto;
          `}
          onClick={() => {
            removePokemon(nickname);
          }}
        >
          Release
        </Button>
      </CardContent>
    </CardItem>
  );
}
