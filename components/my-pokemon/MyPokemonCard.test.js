import TestRenderer from "react-test-renderer";

import { MockedProvider } from "@apollo/client/testing";
import MyPokemonCard, { GET_POKEMON } from "./MyPokemonCard";

const mocks = [
  {
    request: {
      query: GET_POKEMON,
      variables: {
        name: "bulbaaaaasaur",
      },
    },
    result: {
      data: {
        pokemon: {
          // If pokemon is not found, its name in backend will be null
          name: null,
        },
      },
    },
  },
];

it("Displays notice if pokemon not found", async () => {
  let component;
  TestRenderer.act(() => {
    component = TestRenderer.create(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MyPokemonCard name="bulbaaaaasaur" />
      </MockedProvider>
    );
  });

  await TestRenderer.act(
    async () => new Promise((resolve) => setTimeout(resolve, 0))
  );

  const root = component.root;
  const notice = root.findByProps({ "id": "error-notice" });
  expect(notice.props.children).toContain("This pokemon can't be displayed");
});

it("Displays notice if network/GQL error", async () => {
  const errorMock = {
    request: {
      query: GET_POKEMON,
      variables: {
        name: "bulbaaaaasaur",
      },
    },
    error: new Error("An error occurred"),
  };

  let component;
  TestRenderer.act(() => {
    component = TestRenderer.create(
      <MockedProvider mocks={[errorMock]} addTypename={false}>
        <MyPokemonCard name="bulbaaaaasaur" />
      </MockedProvider>
    );
  });

  await TestRenderer.act(
    async () => new Promise((resolve) => setTimeout(resolve, 0))
  );

  const root = component.root;
  const notice = root.findByProps({ id: "error-notice" });
  expect(notice.props.children).toContain("Something went wrong");

  // If this refetch button is not found, this will throw error
  root.findByProps({ id: "refetch" });
});
