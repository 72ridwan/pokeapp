import { css } from "@emotion/react";
import { useEffect, useRef } from "react";

export default function Navbar(props) {
  const navRef = useRef(null);
  const spacerRef = useRef(null);

  useEffect(() => {
    if (navRef.current && spacerRef.current) {
      const navElement = navRef.current;
      const spacerElement = spacerRef.current;

      function adjustSpacerHeight() {
        const height = navElement.offsetHeight;
        spacerElement.style.height = `${height}px`;
      }

      adjustSpacerHeight();

      const observer = new ResizeObserver(adjustSpacerHeight);
      observer.observe(navElement);

      return () => {
        observer.unobserve(navElement);
      };
    }
    return () => {};
  }, [navRef, spacerRef]);

  return (
    <>
      <nav
        {...props}
        ref={navRef}
        css={css`
          display: flex;
          flex-flow: row;
          align-items: center;
          position: fixed;
          top: 0;
          width: 100%;
          min-height: 64px;
          padding: 10px 20px;
          background: #084c7c;
          color: white;
          z-index: 6;
        `}
      />
      {/* Space for navbar */}
      <div
        ref={spacerRef}
        css={css`
          height: 64px;
        `}
      />
    </>
  );
}
