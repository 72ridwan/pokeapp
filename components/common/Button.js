import { css } from "@emotion/react";
import { forwardRef } from "react";

function Button(
  { component: Component = "button", color = "#084c7c", ...otherProps },
  ref
) {
  return (
    <Component
      {...otherProps}
      ref={ref}
      css={css`
        display: block;
        position: relative;
        color: ${color};
        border: thin solid ${color};
        background: none;
        padding: 8px;
        border-radius: 6px;
        margin: 8px 0;
        font-family: inherit;
        overflow: hidden;
        cursor: pointer;

        &::after {
          content: "";
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          display: block;
          opacity: 0;
          background: currentColor;
          transition: 0.2s opacity ease;
        }

        &:hover {
          &::after {
            opacity: 0.1;
          }
        }

        &:active {
          &::after {
            opacity: 0.2;
          }
        }

        &:focus {
          outline: none;

          &::after {
            opacity: 0.3;
          }
        }

        &:disabled {
          border-color: lightgray;
          color: lightgray;
        }
      `}
    />
  );
}

export default forwardRef(Button);
