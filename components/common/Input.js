import { forwardRef } from "react";
import { css } from "@emotion/react";

function Input(props, ref) {
  return (
    <input
      css={css`
        font-size: inherit;
        font-family: inherit;
        border: thin solid #707f89;
        border-radius: 5px;
        padding: 6px 9px;
      `}
      max={96}
      ref={ref}
      {...props}
    />
  );
}

export default forwardRef(Input);
