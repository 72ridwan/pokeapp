import { css } from "@emotion/react";

export default function FooterBar({ footerHeight, ...props }) {
  return (
    <>
      {/* Space for footer */}
      <div style={{ height: footerHeight }} />
      <footer
        {...props}
        css={css`
          position: fixed;
          bottom: 0;
          left: 0;
          width: 100%;
          height: ${footerHeight}px;
          padding: 10px;
          background: #ffffff;
          z-index: 6;
          border-top: 2px solid #828282;
        `}
      />
    </>
  );
}
