import { css, keyframes } from "@emotion/react";

const wave = keyframes`
0% {
  transform: translate3d(-100%, 0, 0);
}
100% {
  transform: translate3d(150%, 0, 0);
}
`;
/** This mimics skeleton animation in Material-UI:
 * https://material-ui.com/components/skeleton/
 */
export default function Skeleton(props) {
  return (
    <div
      css={css`
        display: inline-block;
        position: relative;
        overflow: hidden;
        background-color: #d5d5d5;
        border-radius: 4px;

        & > * {
          opacity: 0;
        }

        &:after {
          position: absolute;
          content: "";
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          animation: ${wave} 1.3s 0.6s linear infinite;
          transform: translateX(-100%);
          background: linear-gradient(
            90deg,
            transparent,
            rgba(255, 255, 255, 0.6),
            transparent
          );
        }
      `}
      {...props}
    />
  );
}
