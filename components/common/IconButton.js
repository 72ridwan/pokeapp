import { css } from "@emotion/react";
import Button from "./Button";

export default function IconButton(props) {
  return (
    <Button
      {...props}
      css={css`
        border: none;
        width: 44px;
        min-width: 44px;
        height: 44px;
        border-radius: 22px;
        display: flex;
        align-items: center;
        justify-content: center;
      `}
    />
  );
}
