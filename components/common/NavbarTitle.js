import { css } from "@emotion/react";

export default function NavbarTitle({ children }) {
  return (
    <div
      css={css`
        font-size: 24px;
        line-height: 1;
        font-weight: 600;
      `}
    >
      {children}
    </div>
  );
}
