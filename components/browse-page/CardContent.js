import { css } from "@emotion/react";

export default function CardContent(props) {
  return (
    <div
      {...props}
      css={css`
        padding: 16px;
        flex-grow: 1;
        display: flex;
        flex-flow: column;
      `}
    />
  );
}
