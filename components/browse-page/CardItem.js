import { css } from "@emotion/react";

export default function CardItem({ children, ...otherProps }) {
  return (
    <li
      {...otherProps}
      css={css`
        background: #ffffff;
        box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
        border-radius: 10px;
        display: flex;
        flex-flow: column;
        overflow: hidden;
      `}
    >
      {children}
    </li>
  );
}
