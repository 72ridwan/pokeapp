import { containerStyle, imageStyle } from "./CardImage.style";

export default function CardImage({ src }) {
  return (
    <div css={containerStyle}>
      <img src={src} css={imageStyle}></img>
    </div>
  );
}
