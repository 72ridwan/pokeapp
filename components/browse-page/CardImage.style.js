import { css } from "@emotion/react";

export const imageStyle = css`
  height: 96px;
  display: block;
  margin: auto;
`;

export const containerStyle = css`
  padding: 10px;
  background: #d3d3d3;
`;
