import { css } from "@emotion/react";

export default function CardList({ children }) {
  return (
    <ul
      css={css`
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
        grid-gap: 16px;

        margin: auto;
        max-width: 750px;

        list-style-type: none;
        padding-inline-start: 0;

        @media (min-width: 500px) {
          grid-template-columns: repeat(auto-fill, minmax(170px, 1fr));
        }
      `}
    >
      {children}
    </ul>
  );
}
