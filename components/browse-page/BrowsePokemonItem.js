import { css } from "@emotion/react";
import { useCallback, useMemo, useState } from "react";
import useStore, { INDEX_NAME } from "@/store";
import { useRouter } from "next/router";

import kebabToTitleCase from "@/utils/kebabToTitleCase";

import CardContent from "./CardContent";
import CardItem from "./CardItem";
import CardImage from "./CardImage";
import { containerStyle, imageStyle } from "./CardImage.style";

const titleStyle = css`
  font-weight: bold;
  font-size: 20px;
  line-height: 1;
  margin: 0;
  margin-bottom: 4px;
`;

export default function BrowsePokemonItem({ image, name }) {
  const pokemons = useStore(
    useCallback(
      (state) => {
        return state.pokemons;
      },
      [name]
    )
  );

  const pokemonCount = useMemo(() => {
    const pokemonWithSameName = pokemons.filter((pokemon) => {
      return pokemon[INDEX_NAME] === name;
    });
    return pokemonWithSameName.length;
  });

  const router = useRouter();
  const goToDetailPage = () => {
    router.push(`/pokemon/${name}`);
  };

  const [randomDeg] = useState(() => parseInt(Math.random() * 360, 10));

  return (
    <CardItem
      tabIndex={0}
      role="link"
      css={css`
        cursor: pointer;
        transition: 0.1s background, 0.1s transform ease;
        word-break: break-word;
        user-select: none;

        &:hover {
          background-color: #efefef;
        }

        &:focus,
        &:focus-within,
        &:active {
          outline: none;
          background-color: #e9eff3;
          box-shadow: 0px 8px 24px rgba(0, 0, 0, 0.25);

          & .css-${imageStyle.name} {
            transform: scale(1.3);
          }
          & .css-${containerStyle.name} {
            background: hsl(${randomDeg}deg 53% 65%);
          }
        }

        & .css-${imageStyle.name} {
          transition: 0.1s transform ease;
        }
        & .css-${containerStyle.name} {
          transition: 0.1s background ease;
        }
      `}
      key={name}
      onClick={goToDetailPage}
    >
      <CardImage src={image} />
      <CardContent
        css={css`
          flex-grow: 1;
          display: flex;
          flex-flow: column;
        `}
      >
        <h5 css={titleStyle}>{kebabToTitleCase(name)}</h5>
        <div
          css={css`
            color: rgba(0, 0, 0, 0.5);
          `}
        >
          Owned: {pokemonCount}
        </div>
        <div
          css={css`
            flex-grow: 1;
            padding: 2px;
          `}
        />
        <span
          css={css`
            text-align: right;
            color: #084c7c;
          `}
        >
          More &gt;
        </span>
      </CardContent>
    </CardItem>
  );
}
