import { useEffect, useRef } from "react";
import { css } from "@emotion/react";
import Button from "@/components/common/Button";

const paginationButton = css`
  margin: 0 8px;
  height: 40px;
  width: 40px;
  border-radius: 100%;
  line-height: 1;
  padding: 0;
`;

function isInBetween(num, min, max) {
  const numAsInt = parseInt(num, 10);
  if (Number.isNaN(numAsInt)) {
    return false;
  }
  return min <= num && max >= num;
}

export default function Pagination({
  page,
  onChangePage,
  itemsPerPage,
  totalItems,
  className,
  hidePage,
}) {
  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.value = page + 1;
    }
  }, [page]);

  const totalPage = Math.floor((totalItems - 1) / itemsPerPage);

  const updatePage = (newPage) => {
    const newPageAsInt = parseInt(newPage, 10);
    if (!isInBetween(newPage, 0, totalPage)) {
      return;
    }
    onChangePage(newPageAsInt);
  };

  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        align-items: center;
      `}
      className={className}
    >
      <Button
        aria-label="Previous page"
        css={paginationButton}
        onClick={() => {
          updatePage(page - 1);
        }}
        disabled={page < 1}
      >
        &lt;
      </Button>
      {!hidePage && (
        <div>
          <label htmlFor="page-input">Page </label>
          <input
            id="page-input"
            ref={inputRef}
            css={css`
              font-size: inherit;
              font-family: inherit;
              padding: 4px;
              width: 4em;
            `}
            type="number"
            defaultValue={page + 1}
            step={1}
            onBlur={(e) => {
              const newPage = parseInt(e.target.value) - 1;
              if (!isInBetween(newPage, 0, totalPage)) {
                e.target.value = page + 1;
              } else {
                updatePage(newPage);
              }
            }}
          />{" "}
          of {totalPage + 1}
        </div>
      )}
      <Button
        aria-label="Next page"
        css={paginationButton}
        onClick={() => {
          updatePage(page + 1);
        }}
        disabled={page >= totalPage}
      >
        &gt;
      </Button>
    </div>
  );
}
